<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $connection = 'mysql2';

    protected $fillable = [
        'LastName', 'FirstName', 'Address', 'City'
    ];
}
