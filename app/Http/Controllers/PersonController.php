<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use Illuminate\Support\Facades\Auth;
use Config;

class PersonController extends Controller
{
    public function list() {
        $persons = Person::all();

        return view('home', compact('persons'));
    }
}
