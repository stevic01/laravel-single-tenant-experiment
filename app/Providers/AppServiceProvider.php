<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Session;
use Config;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Http\Request;
Use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

      
        Config::set('database.connections.mysql2.database', session('key'));
        
    }
}
